
document.addEventListener('DOMContentLoaded', function () {
    const form = document.getElementById('MyForm');
    form.addEventListener('submit', formSend);

    async function formSend(e) {
        e.preventDefault();

        let error = formValidate(form);

        if (error === 0) {
            alert('Отправка выполнена успешно!');
            document.getElementById('MyForm').reset();
        } else {
            alert('Заполните обязательные поля и дайте согласие на обработку персональных данных');
        }
    }

    function formValidate(form) {
        let error = 0;
        let formReq = document.querySelectorAll('._req');

        for (let index = 0; index < formReq.length; index++) {
            const input = formReq[index];
            formRemoveError(input);

            if (input.classList.contains('_email')) {
                if (emailTest(input)) {
                    formAddError(input);
                    error++;
                }
            } else if (input.getAttribute("type") === "checkbox" && input.checked === false) {
                formAddError(input);
                error++;
            } else {
                if (input.value === '') {
                    formAddError(input);
                    error++;
                }
            }
        }
        return error;
    }

    function formAddError(input) {
        input.parentElement.classList.add('_error');
        input.classList.add('_error');
    }

    function formRemoveError(input) {
        input.parentElement.classList.remove('_error');
        input.classList.remove('_error');
    }

    function emailTest(input) {
        return !/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,8})+$/.test(input.value);
    }


});

function open_form() {
    history.pushState(null, null, "#form");
    document.getElementById("popup").style.opacity = 1;
    document.getElementById("popup").style.visibility = "visible";
    localStorage.setItem('11', localStorage.getItem('10'));
    localStorage.setItem('21', localStorage.getItem('20'));
    localStorage.setItem('31', localStorage.getItem('30'));
    document.getElementById('formName').innerHTML = localStorage.getItem('11');
    document.getElementById('formEmail').innerHTML = localStorage.getItem('21');
    document.getElementById('formAgreement').innerHTML = localStorage.getItem('31');
}

function look() {
    var obj = {
        item1: document.getElementById('formName'),
        item2: document.getElementById('formEmail'),
        item3: document.getElementById('formAgreement')
    };
    console.log(obj.item1.value);
    console.log(obj.item2.value);
    console.log(obj.item3.value);
    localStorage.setItem('10', obj.item1.value);
    localStorage.setItem('20', obj.item2.value);
    localStorage.setItem('30', obj.item3.value);
    console.log(localStorage.getItem('10'));
}

function popupClose(popupActive) {
    popupActive.classList.remove('open');
}

window.addEventListener("popstate", function (e) {
    if (location.hash != "#form" || location.reload()) {
        document.getElementById("popup").style.opacity = 0;
        document.getElementById("popup").style.visibility = "hidden";
        look();
    }
});
